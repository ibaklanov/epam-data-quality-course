echo -e "\033[31mЗадание 1.1\nСкопируйте текстовый файл task_1_data на свою виртуальную машину. Это можно сделать, например, с использованием протокола SCP - https://ru.wikipedia.org/wiki/SCP
С использованием grep из файла task1 выведите на экран только целые слова (т.е. вывод только результатов поиска, а не всего содержимого файла с подсвеченными найденными результатами), содержащие 5 и более символов и состоящие только из букв верхнего регистра.\033[0m"

# TESTDATA="first SECOND thirD FOurth FIFTH SIXTH SEVEntH EIGHTH NINEth TEN"
# echo "$TESTDATA" > testfile1
egrep -o "[A-Z]{5,}" task_1_data

echo -e "\033[31mЗадание 1.2\nСоздайте на своей виртуальной машине текстовый файл следующего содержания:
Moscow
Saint-Petersburg
Novosibirsk
Yekaterinburg
Nizhny Novgorod
Kazan
Chelyabinsk
Omsk
Samara
Rostov-on-Don
Ufa
Krasnoyarsk
Perm
Voronezh
Volgograd
Выведите на экран только те города, в названии которых нет буквы a.\033[0m"

CITIES="Moscow
Saint-Petersburg
Novosibirsk
Yekaterinburg
Nizhny Novgorod
Kazan
Chelyabinsk
Omsk
Samara
Rostov-on-Don
Ufa
Krasnoyarsk
Perm
Voronezh
Volgograd"

echo "$CITIES" > testfile2
egrep -o ".*[a|A].*" testfile2

echo -e "\033[31mЗадание 2.1\nВыведите на экран наиболее часто встречающийся user agent (браузер) и число его вхождений в логе.
Пример вывода: 248 Opera/9.80 (Windows NT 5.1; U; ru) Presto/2.9.168 Version/11.51 (здесь: 248 - число вхождений, остальная строка - содержимое поля user agent).\033[0m"
# print $(NF-1); 
awk -F\" '{ 
    userAgent=$(NF-1);
    agentsArray[userAgent]+=1;
    }    
    END {
        maxCount=0;
        mostFreqUserAgent="none";
        for(userAgent in agentsArray){
            if(agentsArray[userAgent] > maxCount){
                maxCount = agentsArray[userAgent];
                mostFreqUserAgent = userAgent;
            }        
    }
    print maxCount " " mostFreqUserAgent
}' access_log

echo -e "\033[31mЗадание 3.1\nПри выводе на экран замаскируйте все IP-адреса фразой "IP address" для всех запросов со смартфона Nexus 5X.\033[0m"
egrep "Nexus 5X" access_log > access_log3.1
# sed 'Nexus 5X/s/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/IP address/w access_log2' access_log
# sed -i 's/[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/IP address/g' access_log2
# sed -n '/Nexus 5X/s/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/IP address/w access_log3.1' access_log

sed 's/^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}/IP address/' access_log3.1

echo -e "\033[31mЗадание 3.2\nЗамените все наименования user agent на "EPAM" с перезаписью файла access_log.\033[0m"

sed -n 's/\"[^\"]*\"/EPAM/3w access_log3.2' access_log