#!/bin/bash

echo -e "\033[31mЗадание 1.1\nОдной командой выведите на экран все файлы, содержащие в имени слово update, из директорий /usr/bin и /usr/sbin.\033[0m"

ls  /usr/sbin/*update* /usr/bin/*update*

echo -e "\033[31mЗадание 1.2\nОдной командой выведите на экран все файлы, которые расположены в дочерних каталогах /usr/share/man/manX (хранилище встроенной документации, разбитое по секциям разделов помощи - man1, man2 ...) и содержат в своем имени слово mount.\033[0m"

ls /usr/share/man/man?/*mount*

echo -e "\033[31mЗадание 1.3\nОдной командой выведите на экран все файлы, содержащие в своем имени слово read, хранящиеся только в трех директориях: /usr/share/man/man1, /usr/share/man/man3 и /usr/share/man/man7.\033[0m"

ls /usr/share/man/man[1,3,7]/*read*

echo -e "\033[31mЗадание 2.1\nИзучите утилиту find, предназначенную для поиска файлов по заданным критериям. С помощью find найдите в /usr/share/man все файлы, содержащие в своем названии слово change.\033[0m"

find /usr/share/man -depth -type f -name *change* 

echo -e "\033[31mЗадание 2.2\nС помощью find найдите в /usr/share/man все файлы, имя которых начинается с time.\033[0m"

find /usr/share/man -type f -name time*

echo -e "\033[31mЗадание 2.3\nС помощью find найдите все скрытые файлы по пути, начинающемся с /etc/cron.\033[0m"

find /etc/cron* -type f -name ".*"

echo -e "\033[31mЗадание 2.4\nКакие ещё действия можно выполнять с найденными файлами (не способ поиска, а действия, применяемые к результатам поиска), используя find? Приведите любой пример с комментарием.\033[0m"
echo -e "\033[33mМожно создать копию файлов из выборки\033[0m"

find /usr/share/man -type f -name time* -exec cp {} /testdir \;

echo -e "\033[31mЗадание 3.1\nИзучите утилиты head и tail, предназначенные для вывода первых и последних строк указанных файлов соответственно. Выведите на экран первые шесть строк файла /etc/apt/sources.list. Выведите на экран последние две строки файла /etc/fstab\033[0m"

head -6 /etc/apt/sources.list; tail -2 /etc/fstab

echo -e "\033[31mЗадание 3.2\nЧто произойдёт, если мы попытаемся запросить вывести больше строк, чем есть в файле? Попробуйте выполнить это на примере с помощью команды wc для файла /etc/apt/apt.conf.d/70debconf.\033[0m"
echo -e "\033[33mВыведутся все строки из файла\033[0m"

wc /etc/apt/apt.conf.d/70debconf -l
head -4 /etc/apt/apt.conf.d/70debconf

echo -e "\033[31mЗадание 4.1\nНаходясь в вашей домашней директории, перейдите в каталог /opt. Перечислите как можно больше различных вариантов, с помощью которых вы сможете вернуться обратно в ваш домашний каталог. Разными вариантами также будут считаться различные относительные пути.\033[0m"
echo "cd
cd -
cd /home/ivan
cd /../home/ivan
cd $HOME
cd ~
cd ~ivan"

echo -e "\033[31mЗадание 5.1\nВ вашем домашнем каталоге создайте директорию task5 и поместите в неё следующие файлы: chapter1.txt, chapter2.txt, chapter3.txt и chapter4.txt. Используя {}, выполните следующие переименования файлов, находясь в домашней директории:
chapter1.txt -> chapter1.md
chapter2.txt -> chapter2
chapter3.txt -> testfile3.txt
chapter4.txt -> file.log\033[0m"

# function rename() {
#     case $1 in
#         chapter1.txt)
#             mv -i $1 "chapter1.md"
#             ;;
#         chapter2.txt)
#             mv -i $1 "chapter2"
#             ;;
#         chapter3.txt)
#             mv -i $1 "testfile3.txt"
#             ;;
#         chapter4.txt)
#             mv -i $1 "file.log"
#             ;;
#     esac
# }

HOMEDIR=~/epam/task1
cd $HOMEDIR ; mkdir task5;
TASKDIR=$HOMEDIR/task5 
touch task5/chapter{1..4}.txt
mv -i "$TASKDIR/chapter1.txt" "$TASKDIR/chapter1.md"
mv -i "$TASKDIR/chapter2.txt" "$TASKDIR/chapter2"
mv -i "$TASKDIR/chapter3.txt" "$TASKDIR/testfile3.txt"
mv -i "$TASKDIR/chapter4.txt" "$TASKDIR/file.log"
# FILES=$(ls $HOMEDIR/task5)
# for FILE in FILES
# do 
#     rename "$FILE"
# done

